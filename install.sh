PACKAGE_NAME=$1;
TMP_PATH="/home/$USER/tmp/$PACKAGE_NAME";
FINAL_PATH="/home/$USER/bin/$PACKAGE_NAME";
BIN_PATH="/home/$USER/bin/$PACKAGE_NAME/usr/bin";
function errclean {
    echo "There was an error while trying to install package:'$PACKAGE_NAME'...";
    rmdir $TMP_PATH;
    exit;
}
trap errclean ERR ;

if [ -z "$1" ]; then
    echo "Usage: $0 <package_name>" >&2 ;
    exit 1;
fi

echo "Creating temp directory: " $TMP_PATH;
mkdir -p $TMP_PATH;
cd $TMP_PATH;
echo "Downloading $PACKAGE_NAME";
apt-get download $1;
echo "Extracting deb file";
ar x *.deb;
echo "Extracting data tar";
tar -xf data.tar.*;

if [ ! -d "$TMP_PATH/usr/bin" ]; then
    echo "There doesn't seem to be a bin directory, so this is unlikely to work.";
    echo "I will leave temp files at $TMP_PATH if you would like to attempt a ";
    echo "manual repair";
    exit 1;
fi;
echo "Preparing final directory: $FINAL_PATH";
mkdir -p $FINAL_PATH; 
echo "Deleting tmp files";
rm *.tar.g? *.deb -f;
echo "Moving files to final directory";
mv * ~/bin/$PACKAGE_NAME;
echo "Deleting temp directory";
rmdir $TMP_PATH;
echo "Adding bin path to app setup file";
printf "#$PACKAGE_NAME - installed $(date) by $USER\nPATH="'$PATH'":$BIN_PATH\n">> "/home/$USER/.app_setup";
echo "";
echo "Everything seems to have worked. If you haven't already, add the following to the bottom of your .profile file:"
echo '. .app_setup';
echo "and then reload your profile by relogging or using the source command";
