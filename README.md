#Overview
This is a basic script for the purposes of installing and running applications on an environment without administrative rights. I have tested it with many applications on Ubuntu 14.04, for the most part it worked. 

Note: Some applications don't follow standard directory structures or have other dependencies outside of their own .deb file. 

Any issues, let me know and I will try and fix it, or just send me a pull request.

#Installation
Installing is relatively straightforward, but incase you are new to this, here is how you do it.

1. Clone this repository: `git clone git@bitbucket.org:/bituser12/usermode-installer`
2. Change into directory: `cd usermode-installer`
3. Install and extract: `bash install.sh <package_name>`
4. Add bin directories to your profile file: `echo . .app_setup >> ~/.profile`. Note that this will run a script on startup which adds all of the application bin directories to your path. 
5. Reload `.profile` file. The easiest way to do this is to just restart your computer. If you can't restart, look into `source`.

##Defaults
 - All applications are installed to `~/bin` in their own folder.
 - PATH setup occurs in `~/.app_setup`. Comments are included which show the package name and date of installation. Paths are added individually so you can manually switch off individual applications.
 - During installation, the `~/tmp` directory is used. In most cases the installer will cleanup after itself.
 - To uninstall, just delete the folder from `~/bin` and remove the associated line from `~/.app_setup`

#Confirmed working applications
 - vim
 - screen
 - tmux
 - git

I take no responsiblity for the horrific damage this script may do to your computer.



~Sam
